import _thread as thread
import os
import random
import re

from pylofi import Player, Youtube


class Gui:
    """
    UI and stuff
    """

    def __init__(self):
        self.youtube = Youtube()
        self.player = Player()

        self.current = None
        self.is_new = True
        self.author = ""
        self.title = ""

        self.youtube.extract_one()

    def random_lofi(self):
        """event to play a new lofi"""
        if self.youtube.last_videos:
            self.youtube.get_random()
            self.is_new = True

    def goback(self):
        """go to the previous lofi stream"""
        self.current = self.youtube.last_videos.pop(-1)
        self.youtube.last_videos.insert(0, self.current)
        self.youtube.video = self.current
        self.is_new = True

    def swap_info(self):
        """update text labels"""
        if not self.youtube.last_videos:
            print("...")
        elif self.is_new:
            self.current = self.youtube.video
            self.author = "".join(filter(str.isascii, self.current["uploader"]))
            self.title = "".join(filter(str.isascii, self.current["title"]))

            self.player.play(self.current["formats"][0]["url"])
            self.is_new = False

    def scroll(self, dt):
        if self.is_new is False:
            print(self.label_title.text[1:] + self.label_title.text[0])


def main():
    gui = Gui()
    gui.random_lofi()
    while True:
        gui.swap_info()


if __name__ == "__main__":
    main()
