from difflib import SequenceMatcher

import youtube_dl

from pylofi import Youtube


def main():
    new_link_list = []
    yt = Youtube()
    yt.ydl = youtube_dl.YoutubeDL({"ignoreerrors": True})
    with open(yt.path, "rb") as md_file:
        txt = md_file.read().decode()

    for link in yt.links:
        print("Test link {}".format(link))
        try:
            v = yt.ydl.extract_info(link, download=False, process=False)
            if not v:
                raise Exception("DownloadError")
        except Exception:
            print("\tfailed")
            for line in txt.split("\n"):
                if link in line:
                    line = line.split("]")[0]
                    line = line.split("[")[-1]
                    break
            try:
                print(line)
            except UnicodeEncodeError:
                print("".join(filter(str.isascii, line)))

            v = yt.ydl.extract_info("ytsearch:{}".format(line), download=False)
            # v = yt.ydl.extract_info(v["webpage_url"], download=False, process=False)

        else:
            print("\tsuccess")

        if "entries" in v.keys():
            entries = list(v["entries"])
            for e in entries:
                if not e:
                    continue
                print(
                    "".join(filter(str.isascii, e["title"])),
                    SequenceMatcher(None, line, e["title"]).ratio(),
                )
                if SequenceMatcher(None, line, e["title"]).ratio() > 0.75:
                    title = e["title"]
                    # url = "https://www.youtube.com/watch?v=" + e["url"]
                    url = e["webpage_url"]

                    break
            else:
                print("no good title found")
                continue

            # print("---->", title, url)
            # title = entries[0]["title"]
        else:
            title = v["title"]
            url = v["webpage_url"]
        new_link_list.append((title, url))

    print("".join(filter(str.isascii, str(new_link_list))))
    with open(yt.path + ".bak", "wb") as md_file:
        md_file.write(txt.encode())

    txt = "#lofi update\n"
    for title, url in new_link_list:
        txt += f"[{title}]({url})\n"

    # print(txt)

    with open(yt.path, "wb") as md_file:
        md_file.write(txt.encode())


if __name__ == "__main__":
    main()
